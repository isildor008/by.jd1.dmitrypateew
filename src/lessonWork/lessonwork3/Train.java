package lessonWork.lessonwork3;

/**
 * Created by isild on 23.03.2017.
 */
public class Train extends Transport {
    private int wagonCount;
   private static int countenRailway;
    static final int AMR_KF=3;


    {
        wagonCount=10;
    }

    {
        countenRailway++;
    }

    public Train(int wagonCount) {
        super();
        this.wagonCount = wagonCount;
    }
    public Train(){

    }

    public static int getCountenRailway() {
        return countenRailway;
    }

    public int getWagonCount() {
        return wagonCount;
    }

    public void setWagonCount(int wagonCount) {
        this.wagonCount = wagonCount;
    }

    @Override
    public double amortisate(int years) {

        double result=(totalRace()*years*AMR_KF)/PERCENT;
        return result;
    }
}
