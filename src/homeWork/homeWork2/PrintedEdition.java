package homeWork.homeWork2;
public abstract class PrintedEdition {

	private double price;
	private String title;
	private String author;

	public PrintedEdition() {
		super();
	}

	public PrintedEdition(double price, String title,String author) {
		this.price = price;
		this.title = title;
		this.author=author;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getPrice() {
		return price;
	}
	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public abstract String printFormat();

	public abstract String printAuthor();



}
