package lessonWork.lessonwork4.page87var11.selection;

import lessonWork.lessonwork4.page87var11.Constants;
import lessonWork.lessonwork4.page87var11.entity.ParkBus;
import lessonWork.lessonwork4.page87var11.util.AddNewDatesBuses;

/**
 * Created by isild on 25.03.2017.
 */
public class Selection {

   public ParkBus numberTreckSelection() {
       ParkBus parkBus = AddNewDatesBuses.addBusInArray();
       for(int i=0;i<parkBus.getSize();i++) {
           if (Constants.NUMBER_TRACK == parkBus.getBusTreckNumber(i)) {
               System.out.println(parkBus.getBusById(i));
           }
       }

       return parkBus;
   }

    public ParkBus wearSelection(){
        ParkBus parkBus = AddNewDatesBuses.addBusInArray();
        for(int i=0;i<parkBus.getSize();i++) {
            if (Constants.YEAR < parkBus.getBusYearWear(i)) {
                System.out.println(parkBus.getBusById(i));
            }
        }
        return parkBus;
    }

    public ParkBus mileageSelection(){
        ParkBus parkBus = AddNewDatesBuses.addBusInArray();
        for(int i=0;i<parkBus.getSize();i++) {
            if (Constants.MILEARGE < parkBus.getBusMileage(i)) {
                System.out.println(parkBus.getBusById(i));
            }
        }
        return parkBus;
    }
}
