package lessonWork.lessonwork4.page87var11.util;

import lessonWork.lessonwork4.page87var11.entity.Bus;
import lessonWork.lessonwork4.page87var11.entity.ParkBus;

import java.util.List;

/**
 * Created by isild on 25.03.2017.
 */
public class AddNewDatesBuses {

    public static ParkBus addBusInArray()  {

        ParkBus parkBus=new ParkBus();
        Bus bus = new Bus();
        bus.setNameDriver("Vasilij");
        bus.setNumberBus(1);
        bus.setNumberTreck(13);
        bus.setCarBrend("Mercedes");
        bus.setYearWear(1998);
        bus.setMileage(505234);
        parkBus.addBus(bus);

        Bus bus2 = new Bus();
        bus2.setNameDriver("Ivan");
        bus2.setNumberBus(1);
        bus2.setNumberTreck(13);
        bus2.setCarBrend("Setra");
        bus2.setYearWear(2005);
        bus2.setMileage(300734);
        parkBus.addBus(bus2);

        Bus bus3 = new Bus();
        bus3.setNameDriver("Oleg");
        bus3.setNumberBus(9);
        bus3.setNumberTreck(48);
        bus3.setCarBrend("VW");
        bus3.setYearWear(1986);
        bus3.setMileage(1390734);
        parkBus.addBus(bus3);

        Bus bus4 = new Bus();
        bus4.setNameDriver("Lesha");
        bus4.setNumberBus(89);
        bus4.setNumberTreck(9);
        bus4.setCarBrend("Tiguan");
        bus4.setYearWear(2010);
        bus4.setMileage(189734);
        parkBus.addBus(bus4);

        return parkBus ;
    }
}
