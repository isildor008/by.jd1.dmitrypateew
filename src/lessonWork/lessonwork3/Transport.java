package lessonWork.lessonwork3;

/**
 * Created by isild on 23.03.2017.
 */
public abstract class  Transport {
    private double massa;
   private static String producer;
    private double race;
  protected  static final  int PERCENT=100;
    {
        massa=1;
    }

    public Transport(double massa) {
        super();

        this.massa = massa;
    }

    public Transport() {

    }


    public double getMassa() {
        return massa;
    }

    public void setMassa(double massa) {
        this.massa = massa;
    }

    public double totalRace(){
        return race;
    }

    public void addDistance(double distance){
        race+=distance;
    }
    public abstract double amortisate(int years);
}
