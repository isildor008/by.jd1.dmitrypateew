package homeWork.homeWork3.image;

/**
 * Created by isild on 19.03.2017.
 */
public class ViewMenu {
    public static String viewMenu() {
        System.out.println("\nPlease choose option:\n" +
                "1-Selection in ascending order\n" +
                "2-Selection in descending order\n" +
                "3-Find min value\n" +
                "4-Find man value\n" +
                "5-Add new element in massif\n" +
                "6-Show massif\n"+
                "7-Exit Program\n");
        return String.valueOf("");
    }
}
