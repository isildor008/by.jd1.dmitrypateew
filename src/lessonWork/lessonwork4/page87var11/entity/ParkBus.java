package lessonWork.lessonwork4.page87var11.entity;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by isild on 25.03.2017.
 */
public class ParkBus {

    private List<Bus> parkBus = new ArrayList();

    public ParkBus() {

    }

    public List<Bus> getParkBus() {
        return parkBus;
    }

    public void setParkBus(List<Bus> parkBus) {
        this.parkBus = parkBus;
    }

    public Bus addBus(Bus bus) {
        parkBus.add(bus);
        return bus;
    }


    public int getBusTreckNumber(int i) {
        return parkBus.get(i).getNumberTreck();
    }

    public Bus getBusById(int i) {
        return parkBus.get(i);
    }

    public int getSize() {
        return parkBus.size();
    }

    public int getBusYearWear(int i)
    {
        return parkBus.get(i).getYearWear();
    }

    public int getBusMileage(int i)
    {
        return parkBus.get(i).getMileage();
    }


    @Override
    public String toString() {
        return "ParkBus{" +
                "parkBus=" + parkBus +
                '}';
    }
}
