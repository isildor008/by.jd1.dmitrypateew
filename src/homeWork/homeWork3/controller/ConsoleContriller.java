package homeWork.homeWork3.controller;

import homeWork.homeWork3.image.ViewMenu;
import homeWork.homeWork3.massif.Massif;

import java.util.Scanner;

/**
 * Created by isild on 21.03.2017.
 */
public class ConsoleContriller {
    public void start(){
        ViewMenu viewMenu = new ViewMenu();
        Massif massif = new Massif();
        LOOP: for (; ; ) {
            System.out.println(viewMenu.viewMenu());
            Scanner scanner = new Scanner(System.in);
            int choose = 0;
            try {
                choose = scanner.nextInt();
            } catch (Exception e) {
            }
            switch (choose) {
                case 1:
                    massif.ascendingSort();
                    break;
                case 2:
                    massif.descendingSort();
                    break;
                case 3:try {
                    System.out.println("Max value="+massif.getMinValue());
                } catch (Exception e) {
                    System.out.println("Massif is empty. Please, input element in massif!");
                }
                    break;
                case 4:try {
                    System.out.println("Max value=" + massif.getMaxValue());
                } catch (Exception e) {
                    System.out.println("Massif is empty. Please, input element in massif!");
                }break;
                case 5:
                    System.out.print("Input new element=");
                    massif.addNewElement();
                    break;
                case 6:
                    massif.viewMassif();
                    break;
                case 7:
                    System.out.print("Exit Program");
                    break LOOP;
                default:
                    System.out.print("Please, select option from 1 to 7.");
            }
        }
    }
}
