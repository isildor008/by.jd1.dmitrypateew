package homeWork.homework4a.libary;

/**
 * Created by isild on 24.03.2017.
 */
public class Book {

    private int id;
    private int year;
    private String bookName;
    private String author;

    public Book() {
    }

    public Book(int id, String bookName, int year, String author) {
        this.id = id;
        this.author = author;
        this.bookName = bookName;
        this.year = year;
    }


    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", year=" + year +
                ", bookName='" + bookName + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}

