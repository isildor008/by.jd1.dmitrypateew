package lessonWork.lessonwork4.page87var11.entity;

/**
 * Created by isild on 25.03.2017.
 */
public class Bus {
    private String nameDriver;
    private int numberBus;
    private int numberTreck;
    private String carBrend;
    private int yearWear;
    private int mileage;

    public Bus(String nameDriver, int numberBus, int numberTreck, String carBrend, int yearWear, int mileage) {
        this.nameDriver = nameDriver;
        this.numberBus = numberBus;
        this.numberTreck = numberTreck;
        this.carBrend = carBrend;
        this.yearWear = yearWear;
        this.mileage = mileage;
    }

    public Bus() {
    }

    public String getNameDriver() {
        return nameDriver;
    }

    public void setNameDriver(String nameDriver) {
        this.nameDriver = nameDriver;
    }

    public int getNumberBus() {
        return numberBus;
    }

    public void setNumberBus(int numberBus) {
        this.numberBus = numberBus;
    }

    public int getNumberTreck() {
        return numberTreck;
    }

    public void setNumberTreck(int numberTreck) {
        this.numberTreck = numberTreck;
    }

    public String getCarBrend() {
        return carBrend;
    }

    public void setCarBrend(String carBrend) {
        this.carBrend = carBrend;
    }

    public int getYearWear() {
        return yearWear;
    }

    public void setYearWear(int yearWear) {
        this.yearWear = yearWear;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "nameDriver='" + nameDriver + '\'' +
                ", numberBus=" + numberBus +
                ", numberTreck=" + numberTreck +
                ", carBrend='" + carBrend + '\'' +
                ", yearWear=" + yearWear +
                ", mileage=" + mileage +
                '}';
    }
}
