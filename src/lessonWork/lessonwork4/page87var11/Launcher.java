package lessonWork.lessonwork4.page87var11;
import lessonWork.lessonwork4.page87var11.selection.Selection;
import lessonWork.lessonwork4.page87var11.util.AddNewDatesBuses;

/**
 * Created by isild on 25.03.2017.
 */
public class Launcher {
    public static void main(String[] args) {
        System.out.println(AddNewDatesBuses.addBusInArray());
        Selection selection=new Selection();
        System.out.println("Selection bus by track number: ");
        selection.numberTreckSelection();
        System.out.println("Selection bus by year wear: ");
        selection.wearSelection();
        System.out.println("Selection bus by mileage:");
        selection.mileageSelection();

    }
}
