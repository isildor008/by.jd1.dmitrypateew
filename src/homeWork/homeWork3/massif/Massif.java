package homeWork.homeWork3.massif;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by isild on 18.03.2017.
 */
public class Massif {

    private int[] massif = new int[0];

    public int[] addNewElement() {
        try {
            Scanner scanner = new Scanner(System.in);
            int newElement = scanner.nextInt();
            massif = addElement(massif, newElement);
            System.out.print("New Element=" + newElement + "\n");
        } catch (Exception e) {
            System.out.println("Incorrect input value!");
        }
        return massif;
    }

    private int[] addElement(int[] a, int e) {
        a = Arrays.copyOf(a, a.length + 1);
        a[a.length - 1] = e;
        return a;
    }

    public void viewMassif() {
        if (massif.length == 0) {
            System.out.println("Massif is empty. Please, input element in massif");
        }
        for (int n = 0; n < massif.length; n++) {
            System.out.print(massif[n]);
        }
        System.out.println("\n");
    }

    public int[] ascendingSort() {
        if (massif.length == 0) {
            System.out.println("Massif is empty. Please, input element in massif");
            return massif;
        }
        System.out.print("Massif is sorted in ascending order.");
        for (int i = 0; i < massif.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < massif.length; j++)
                if (massif[j] < massif[index])
                    index = j;
            int smallerNumber = massif[index];
            massif[index] = massif[i];
            massif[i] = smallerNumber;
        }
        return massif;
    }

    public int[] descendingSort() {
        if (massif.length == 0) {
            System.out.println("Massif is empty. Please, input element in massif");
            return massif;
        }
        System.out.print("Massif is sorted in descending order.");
        for (int i = 0; i < massif.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < massif.length; j++)
                if (massif[j] > massif[index])
                    index = j;
            int biggerNumber = massif[index];
            massif[index] = massif[i];
            massif[i] = biggerNumber;
        }
        return massif;
    }

    public int getMinValue() {
           int minValue= massif[0];
             for (int i = 1; i < massif.length; i++) {
            if (massif[i] < minValue) {
                minValue = massif[i];
                System.out.println("Max value=" +minValue);

            }
        }return minValue;

    }

    public int getMaxValue() {
        int maxValue = massif[0];
        for (int i = 1; i < massif.length; i++) {
            if (massif[i] > maxValue) {
                maxValue = massif[i];
            }
        }
        return maxValue;
    }
}
