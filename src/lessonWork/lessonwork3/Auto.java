package lessonWork.lessonwork3;

/**
 * Created by isild on 23.03.2017.
 */
public class Auto extends Transport {
    private int passangers;
    private static int count_auto;
    static final int AMR_KF=10;

    {
        passangers = 4;

    }

    {
        count_auto++;
    }

    public Auto(int passangers) {
        super();
        this.passangers = passangers;
    }

    public Auto(){
        super();

    }


    public int getPassangers() {
        return passangers;
    }

    public void setPassangers(int passangers) {
        this.passangers = passangers;
    }


    @Override
    public double amortisate(int years) {
        double result=totalRace()*AMR_KF/PERCENT;
        return result;
    }
}
